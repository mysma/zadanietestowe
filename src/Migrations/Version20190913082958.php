<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
final class Version20190913082958 extends AbstractMigration
{

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return 'add messages table';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
       $this->addSql('CREATE TABLE messages (
            `id` INT NOT NULL AUTO_INCREMENT,
            `context` VARCHAR(45) NULL,
            PRIMARY KEY (`id`));'
       );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM messages');
    }
}
