<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
final class Version20190913083212 extends AbstractMigration
{

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return 'Add relation';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE emails 
            ADD CONSTRAINT `fk_email`
            FOREIGN KEY (`message_id`)
            REFERENCES messages (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
        ');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE emails
            DROP FOREIGN KEY `fk_email`;
            ALTER TABLE emails 
            DROP INDEX `message_id_UNIQUE` ;
        ');
    }
}
