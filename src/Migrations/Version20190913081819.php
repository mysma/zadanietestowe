<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
final class Version20190913081819 extends AbstractMigration
{

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return 'Create emails table';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE emails (
            `id` INT NOT NULL AUTO_INCREMENT,
            `message_id` INT NOT NULL,
            `author_number` VARCHAR(11) NOT NULL,
            `date` DATETIME NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `message_id_UNIQUE` (`message_id` ASC));'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql('	DELETE FROM emails');
    }
}
