<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\iMapService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class EmailController extends Controller
{
    /**
     * @return Response
     */
    public function getUnreadEmailsAction(): Response
    {
        $emailsService = $this->get(iMapService::class);

        $unreadEmails = $emailsService->getUnreadEmailsFromDb();

        if (true === empty($unreadEmails)) {
            return $this->render('messages.html.twig', [
                'messages' => null,
            ]);
        }

        return $this->render('messages.html.twig', [
            'messages' => $unreadEmails,
        ]);
    }
}