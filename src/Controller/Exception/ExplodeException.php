<?php

declare(strict_types=1);

namespace App\Controller\Exception;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class ExplodeException extends \Exception
{
}