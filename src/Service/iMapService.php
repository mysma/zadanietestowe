<?php

declare(strict_types=1);

namespace App\Service;

use App\Controller\Exception\ExplodeException;
use App\Entity\Emails;
use App\Entity\Messages;
use App\Enum\iMapCriteriaMethodEnum;
use App\Enum\KeyWordCutEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Persisters\PersisterException;
use PhpImap\Exceptions\ConnectionException;
use PhpImap\Mailbox;
use PhpImap\Exceptions\InvalidParameterException;
use Psr\Log\LoggerInterface;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class iMapService
{
    /**
     * @var array
     */
    private $imapConnect;

    /**
     * @var Mailbox
     */
    private $mailBox;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param array $imapConnect
     * @param LoggerInterface $logger
     * @throws InvalidParameterException
     */
    public function __construct(array $imapConnect, LoggerInterface $logger, EntityManager $em)
    {
        $this->imapConnect = $imapConnect;
        $this->mailBox = new Mailbox(
            $this->imapConnect['imapHost'],
            $this->imapConnect['imapLogin'],
            $this->imapConnect['imapPass']
        );
        $this->logger = $logger;
        $this->em = $em;
    }

    /**
     * @return array|null
     * @throws ConnectionException
     * @throws InvalidParameterException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getUnreadEmails()
    {
        if (false === $this->checkConnectionImap()) {
            throw new ConnectionException('Imap connection error!');
        }

        try {
            $unreadEmailsIds = $this->mailBox->searchMailbox(iMapCriteriaMethodEnum::UNREAD);
        } catch (InvalidParameterException $e) {
            $this->logger->error('Error read Inbox: ', [
                $e->getMessage(),
                $e->getTraceAsString(),
                $e->getFile()
            ]);

            throw new InvalidParameterException('Error read Inbox');
        }

        if (true === empty($unreadEmailsIds) || false === is_array($unreadEmailsIds)) {
            return null;
        }

        $result = [];

        foreach ($unreadEmailsIds as $id) {
            $email = strip_tags($this->mailBox->getMail($id)->textHtml);
            if (false === empty($email)) {
                try {

                    $author = explode(
                        KeyWordCutEnum::NADAWCA,
                        preg_split(
                            '@(?='.$this->checkForKeyWord($email, KeyWordCutEnum::getRecipientKey()).')@',
                            $email)[0]
                    )[1];

                    $content = preg_split(
                        '@(?='.$this->checkForKeyWord($email, KeyWordCutEnum::getContentKey()).')@',
                        $email
                    )[1];

                    if (null === $this->checkForKeyWord($content, KeyWordCutEnum::getIronCombination())) {
                        $content = explode(
                            $content,
                            explode($this->checkForKeyWord($content, KeyWordCutEnum::getContentKey()), $content
                            )[1]
                        )[0];
                    }
                    else {
                        $content = explode(
                            $this->checkForKeyWord($content, KeyWordCutEnum::getIronCombination()),
                            explode($this->checkForKeyWord($content, KeyWordCutEnum::getContentKey()), $content
                            )[1]
                        )[1];
                    }

                    $date = new \DateTime($this->mailBox->getMail($id)->date);

                    $result[] = $this->saveEmail($author, $content, $date);

                    $this->mailBox->markMailAsRead($id);
                } catch (ExplodeException $e) {
                    $this->logger->error('Error with cut content', [
                        $e->getMessage(),
                        $e->getTraceAsString(),
                        $e->getFile()
                    ]);
                    $this->mailBox->markMailAsUnread($id);
                } catch (PersisterException $e) {
                    $this->logger->error('Error with persist', [
                        $e->getMessage(),
                        $e->getTraceAsString(),
                        $e->getFile()
                    ]);
                    $this->mailBox->markMailAsUnread($id);
                }
            }
        }

        return $result;
    }

    /**
     * @return Emails[]
     */
    public function getUnreadEmailsFromDb(): array
    {
        return $this->em->getRepository(Emails::class)->findAll();
    }

    /**
     * @return bool
     */
    private function checkConnectionImap(): bool
    {
        try {
            $this->mailBox->checkMailbox();
            return true;
        } catch (ConnectionException $e) {
            $this->logger->error('Connection error: ', [
                $e->getMessage(),
                $e->getFile(),
                $e->getTraceAsString()
            ]);
            return false;
        }
    }

    /**
     * @param $author
     * @param $content
     * @param $date
     * @return Emails
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveEmail($author, $content, $date): Emails
    {
        $message = new Messages();
        $email = new Emails();

        $message->setContext($content);

        $this->save($message);

        $email->setMessageId($message);
        $email->setAuthorNumber($author);
        $email->setDate($date);

        $this->save($email);

        return $email;
    }

    /**
     * @param object $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function save(object $entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param string $string
     * @param array $keys
     * @return string|null
     */
    private function checkForKeyWord(string $string, array $keys): ?string
    {
        foreach ($keys as $value) {
            if (true == strstr($string, $value)) {
                return $value;
            }
        }
        return null;
    }
}