<?php

declare(strict_types=1);

namespace App\Enum;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class KeyWordCutEnum
{
    public const NADAWCA = 'Nadawca:';
    public const ODBIORCA = 'Odbiorca:';
    public const ODBIORCATELEFON = 'Telefon:';
    public const TRESC = 'Treść odebranej wiadomości:';
    public const TRESCWIADOMOSC = 'Treść wiadomości:';
    public const IRON = 'iron';
    public const IRONDOT = '.iron';
    public const IRONBIG = 'Iron';
    public const IRONALLBIG = 'IRON';

    /**
     * @return string[]
     */
    public static function getList(): array
    {
        return [
            self::NADAWCA,
            self::ODBIORCA,
            self::TRESC,
            self::IRON
        ];
    }

    /**
     * @return string[]
     */
    public static function getIronCombination(): array
    {
        return [
            self::IRONDOT,
            self::IRON,
            self::IRONBIG,
            self::IRONALLBIG
        ];
    }

    /**
     * @return string[]
     */
    public static function getContentKey(): array
    {
        return [
            self::TRESCWIADOMOSC,
            self::TRESC,
        ];
    }

    /**
     * @return string[]
     */
    public static function getRecipientKey(): array
    {
        return [
            self::ODBIORCA,
            self::ODBIORCATELEFON,
        ];
    }
}