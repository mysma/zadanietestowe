<?php

declare(strict_types=1);

namespace App\Enum;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class iMapCriteriaMethodEnum
{
    public const UNREAD = 'UNSEEN';
    public const ALL = 'ALL';

    /**
     * @return string[]
     */
    public static function getList(): array
    {
        return [
            self::UNREAD,
            self::ALL
        ];
    }
}