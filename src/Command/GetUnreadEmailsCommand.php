<?php

declare(strict_types=1);


namespace App\Command;

use App\Service\iMapService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class GetUnreadEmailsCommand extends  Command
{
    /**
     * @var iMapService
     */
    private $imapService;

    /**
     * GetUnreadEmailsCommand constructor.
     * @param iMapService $imapService
     */
    public function __construct(iMapService $imapService)
    {
        $this->imapService = $imapService;
        parent::__construct();
    }

    /**
     * @var string
     */
    protected static $defaultName = 'get-unread-emails';

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Get unread emails')
            ->setHelp('This command get unread emails')
        ;
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \PhpImap\Exceptions\ConnectionException
     * @throws \PhpImap\Exceptions\InvalidParameterException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->imapService->getUnreadEmails();
    }

}