<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * @author Maciej Myszkiewicz <m.maciek@op.pl>
 */
class Emails
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var Messages
     */
    private $messageId;

    /**
     * @var string
     */
    private $authorNumber;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Messages
     */
    public function getMessageId(): Messages
    {
        return $this->messageId;
    }

    /**
     * @param Messages $messageId
     */
    public function setMessageId(Messages $messageId): void
    {
        $this->messageId = $messageId;
    }

    /**
     * @return string
     */
    public function getAuthorNumber(): string
    {
        return $this->authorNumber;
    }

    /**
     * @param string $authorNumber
     */
    public function setAuthorNumber(string $authorNumber): void
    {
        $this->authorNumber = $authorNumber;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }
}